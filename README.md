pgbouncer exporter role
=========

Installs pgbouncer exporter for Prometheus on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.pgbouncer_exporter
```
